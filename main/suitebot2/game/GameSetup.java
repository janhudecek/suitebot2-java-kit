package suitebot2.game;

import java.util.List;

public class GameSetup
{
	public final int aiPlayerId;
	public final List<Player> players;
	public final GamePlan gamePlan;

	public GameSetup(int aiPlayerId, List<Player> players, GamePlan gamePlan)
	{
		this.aiPlayerId = aiPlayerId;
		this.players = players;
		this.gamePlan = gamePlan;
	}

	@Override
	public String toString()
	{
		return "GameSetup{" +
				       "aiPlayerId=" + aiPlayerId +
				       ", players=" + players +
				       ", gamePlan=" + gamePlan +
				       '}';
	}
}
